package com.jawise.example;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class PaymentMessageConsumer implements MessageListener,
		ExceptionListener {

	private Session session;
	private Destination paymentq;
	private Destination orderq;
	private boolean running;

	public static void main(String[] args) throws Exception {

		PaymentMessageConsumer c = new PaymentMessageConsumer();
		c.start(args);
	}

	public void start(String[] args) {
		try {

			Properties props = new Properties();

			/*
			 * active mq
			 */
			props.setProperty(Context.INITIAL_CONTEXT_FACTORY,
					"org.apache.activemq.jndi.ActiveMQInitialContextFactory");
			props.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616");

			/**
			 * jboss mq
			 */
			/*
			 * props.setProperty(Context.INITIAL_CONTEXT_FACTORY,
			 * "org.jnp.interfaces.NamingContextFactory");
			 * props.setProperty(Context.PROVIDER_URL, "jnp://localhost:1099");
			 * props.setProperty(Context.URL_PKG_PREFIXES,
			 * "org.jnp.interfaces");
			 */
			InitialContext jndiContext = null;
			try {
				jndiContext = new InitialContext(props);
			} catch (NamingException e) {
				System.out.println("Could not create JNDI API " + "context: "
						+ e.toString());
				System.exit(1);
			}

			/*
			 * Look up connection factory and queue. If either does not exist,
			 * exit.
			 */

			QueueConnectionFactory connectionFactory = null;
			try {
				connectionFactory = (QueueConnectionFactory) jndiContext
						.lookup("QueueConnectionFactory");
			} catch (NamingException e) {
				System.out.println("JNDI API lookup failed: " + e.toString());
				System.exit(1);
			}

			Connection connection = connectionFactory.createConnection();
			connection.setExceptionListener(this);
			connection.start();

			session = connection.createSession(false, 0);
			paymentq = session.createQueue("PaymentProcessingQ");

			MessageConsumer consumer = null;

			for (;;) {
				if (!running) {
					running = true;
					connection = connectionFactory.createConnection();
					connection.setExceptionListener(this);
					connection.start();
					session = connection.createSession(false, 0);
					consumer = session.createConsumer(paymentq);
					consumer.setMessageListener(this);
					System.out.println("started order processing listener");
				}
				Thread.sleep(200);
			}

		} catch (Exception e) {
			System.out.println("Caught: " + e);
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(Message m) {
		try {
			System.out.println("message received");
			TextMessage order = (TextMessage) m;
			sendRespone(order);
		} catch (Exception ex) {

		}
	}

	public void sendRespone(TextMessage order) throws JMSException {

		String messagetypename = order.getStringProperty("messagetypename");
		String queuingorder = order.getStringProperty("queuingorder");
		System.out.println("queuing order : " + queuingorder);


		if ("GenericXmlMessage".equals(messagetypename)) {
			String conversationhandle = order
					.getStringProperty("conversationhandle");
			
			System.out.println("conversationhandle : " + conversationhandle);
			String ordermsg = order.getText();
			// the processing is here
			System.out.println("received: " + ordermsg);
			if (ordermsg.indexOf("<status>paymentpending</status>") >= 0) {
				ordermsg = ordermsg.replaceAll(
						"<status>paymentpending</status>",
						"<status>paymentcompleted</status>");
				System.out
						.println("updated status to : paymentcompleted");
			} else {
				System.out.println("status not correct");
			}
			MessageProducer producer = null;
			orderq = session.createQueue("OrderProcessingQ");
			producer = session.createProducer(orderq);
			TextMessage response = session.createTextMessage();
			response.setText(ordermsg);
			response
					.setStringProperty("conversationhandle", conversationhandle);
			producer.send(response);

		} else {
			System.out.println("message type not processed  :"
					+ messagetypename);
		}

	}

	@Override
	public void onException(JMSException ex) {

		System.out.println("exception received");
		System.out.println("exception msg : " + ex.getMessage());
		System.out.println("stop processing");
		running = false;
	}

}
