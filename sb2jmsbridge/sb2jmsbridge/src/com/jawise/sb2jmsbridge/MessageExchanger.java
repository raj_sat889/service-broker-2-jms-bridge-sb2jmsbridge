package com.jawise.sb2jmsbridge;

import org.apache.log4j.Logger;

abstract class MessageExchanger implements Runnable {

	private static Logger logger = Logger.getLogger(MessageExchanger.class);

	private SbQueueConfiguration sbconfiguaration;
	private JmsQueueConfiguration jmsconfiguration;
	private Thread exchangeThread;
	private boolean stop;
	private boolean running;
	private Exception lastexception;

	public MessageExchanger(SbQueueConfiguration sbsconfig,
			JmsQueueConfiguration jmsconfig) {
		super();
		this.sbconfiguaration = sbsconfig;
		this.jmsconfiguration = jmsconfig;
	}

	public void startExchange() throws MessageExchangerException {
		logger.debug("starting exchange");
		stop = false;
		exchangeThread = new Thread(this);
		exchangeThread.start();
	}

	public void stopExchange() {
		logger.debug("stopping exchange");
		stop = true;
		while (running) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
	}

	public SbQueueConfiguration getSbconfiguaration() {
		return sbconfiguaration;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public void setSbsconfiguaration(SbQueueConfiguration sbsconfiguaration) {
		this.sbconfiguaration = sbsconfiguaration;
	}

	public JmsQueueConfiguration getJmsconfiguration() {
		return jmsconfiguration;
	}

	public void setJmsconfiguration(JmsQueueConfiguration jmsconfiguration) {
		this.jmsconfiguration = jmsconfiguration;
	}

	public Thread getExchangeThread() {
		return exchangeThread;
	}

	public void setExchangeThread(Thread exchangeThread) {
		this.exchangeThread = exchangeThread;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public Exception getLastexception() {
		return lastexception;
	}

	public void setLastexception(Exception lastexception) {
		this.lastexception = lastexception;
	}
}
