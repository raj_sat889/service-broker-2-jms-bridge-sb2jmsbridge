package com.jawise.sb2jmsbridge;

import java.sql.Connection;
import java.util.List;

import javax.jms.TextMessage;
import javax.jms.XASession;
import javax.sql.DataSource;
import javax.sql.XAConnection;
import javax.sql.XADataSource;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

import org.springframework.transaction.jta.JtaTransactionManager;

class XASb2JmsExchanger extends Sb2JmsExchanger {

	public XASb2JmsExchanger(SbQueueConfiguration sbsconfig,
			JmsQueueConfiguration jmsconfig, DataSource dSource) {
		super(sbsconfig, jmsconfig);
		this.dSource = dSource;
	}

	public XASb2JmsExchanger(SbQueueConfiguration sbsconfig,
			JmsQueueConfiguration jmsconfig) {
		super(sbsconfig, jmsconfig);
	}

	@Override
	public void run() {

		try {
			setRunning(true);
			SbMessageReceiver receiver = new SbMessageReceiver(dSource,
					getSbconfiguaration());
			connect();

			Sb2JmsBridge instance = Sb2JmsBridge.getInstance();
			JtaTransactionManager jtaTxManneger = instance
					.getXaTransactionManager();
			DataSource dataSource = instance.getDataSource();
			TransactionManager transactionManager = jtaTxManneger
					.getTransactionManager();
			Transaction transaction = null;

			for (;;) {
				// get message from receiver
				Thread.sleep(100);

				transactionManager.begin();
				transaction = transactionManager.getTransaction();

				XAResource xaresource2 = ((XASession) qSession).getXAResource();
				transaction.enlistResource(xaresource2);
				XAConnection xacon = ((XADataSource) dataSource)
						.getXAConnection();
				XAResource xaresource1 = xacon.getXAResource();
				transaction.enlistResource(xaresource1);

				try {
					// should have begin transaction here
					List<SbMessage> messages = receiver.receive(1000,
							(Connection) xacon.getConnection());
					if (messages != null) {
						for (SbMessage msg : messages) {
							TextMessage textmsg = qSession.createTextMessage();
							createJmsMessage(msg, textmsg);
							messageProducer.send(textmsg);
							logger
									.debug("send message to jms queue :"
											+ getJmsconfiguration().getQueue()
											+ " queuingorder :"
											+ msg.getQueuingorder());
						}

						transactionManager.commit();
					} else {
						transactionManager.rollback();
					}
					xacon.close();

				} catch (Exception ex) {
					logger.debug("rolledback faild exchange");
					try {
						if (transaction != null) {
							transactionManager.rollback();
							transaction = null;
						}
						if (xacon != null)
							xacon.close();

					} catch (Exception ex1) {
						logger.error(ex1.getMessage());
					}

					throw ex;

				}
				if (isStop()) {
					logger.info("stopping Sbs2JmsExchanger");
					break;
				}
			}

		} catch (Exception ex) {
			setLastexception(ex);
			logger.error(ex.getMessage(), ex);
		} finally {
			setStop(false);
			setRunning(false);
			disconnect();
		}

	}
}
