package com.jawise.sb2jmsbridge;

public class MessageDeliveryException extends Exception {

	public MessageDeliveryException() {
		super();
	}

	public MessageDeliveryException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public MessageDeliveryException(String arg0) {
		super(arg0);
	}

	public MessageDeliveryException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
