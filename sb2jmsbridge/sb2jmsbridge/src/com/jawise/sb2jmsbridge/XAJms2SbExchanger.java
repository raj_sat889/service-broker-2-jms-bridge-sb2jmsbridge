package com.jawise.sb2jmsbridge;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.QueueConnectionFactory;
import javax.jms.TextMessage;
import javax.jms.XASession;
import javax.sql.DataSource;
import javax.sql.XAConnection;
import javax.sql.XADataSource;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

import org.springframework.transaction.jta.JtaTransactionManager;

class XAJms2SbExchanger extends Jms2SbExchanger {

	@Override
	public void startExchange() throws MessageExchangerException {

		try {			
			Thread exchangeThread = new Thread(this);
			exchangeThread.start();
		} catch (Exception ex) {
			logger.error(ex.getMessage(),ex);
		}		
	}

	@Override
	public void stopExchange() {
		logger.debug("stopping exhchange");
		setStop(true);
		while (isRunning()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
	}
	public XAJms2SbExchanger(SbQueueConfiguration sbsconfiguration,
			JmsQueueConfiguration jmsconfiguration, DataSource source) {
		super(sbsconfiguration, jmsconfiguration, source);
	}

	protected void connect() throws JMSException {
		logger.debug("connecting to jms provider");
		Sb2JmsBridge sbsJmsBridge = Sb2JmsBridge.getInstance();
		QueueConnectionFactory qConnectionFactory = sbsJmsBridge
				.getQueueConnectionFactory();
		qConnection = qConnectionFactory.createQueueConnection();

		qSession = qConnection.createSession(true, 0);
		queue = qSession.createQueue(getJmsconfiguration().getQueue());
		qConnection.start();
		qConnection.setExceptionListener(this);
		setRunning(true);
	}

	protected void disconnect() {
		logger.debug("disconnecting from jms provider");
		try {

			if (qConnection != null) {
				qConnection.stop();
			}
			if (messageconsumer != null) {
				messageconsumer.close();
			}
			if (qSession != null) {
				qSession.close();
			}
			if (qConnection != null) {
				qConnection.stop();
				qConnection.close();
			}
		} catch (Exception ex) {
			logger.debug(ex.getMessage());
		}
	}

	@Override
	public void onMessage(Message m) {

	}

	@Override
	public void run() {
		try {
			
			logger.debug("start procesing messages");
			setRunning(true);
			SbMessageSender sender = new SbMessageSender(dSource,
					getSbconfiguaration());		
			Sb2JmsBridge instance = Sb2JmsBridge.getInstance();
			JtaTransactionManager jtaTxManneger = instance
					.getXaTransactionManager();
			DataSource dataSource = instance.getDataSource();
			TransactionManager transactionManager = jtaTxManneger
					.getTransactionManager();
			Transaction transaction = null;
			connect();
			for (;;) {
				// get message from receiver
				Thread.sleep(100);
				transactionManager.begin();
				transaction = transactionManager.getTransaction();
				XAResource xaresource2 = ((XASession) qSession).getXAResource();
				transaction.enlistResource(xaresource2);
				XAConnection xacon = ((XADataSource) dataSource)
						.getXAConnection();
				XAResource xaresource1 = xacon.getXAResource();
				transaction.enlistResource(xaresource1);

				try {
					// should have begin transaction here
					messageconsumer = qSession.createConsumer(queue);
					Message m = messageconsumer.receive(10000);
					if (m != null) {
						SbMessage msg = createSbMessage((TextMessage) m);
						sender.send(msg,xacon.getConnection());
						transactionManager.commit();
					} else {
						transactionManager.rollback();
					}
					messageconsumer.close();
					xacon.close();

				} catch (Exception ex) {
					logger.debug("rolledback faild exchange");
					try {
						if (transaction != null) {
							transactionManager.rollback();
							transaction = null;
						}
						if (xacon != null)
							xacon.close();

					} catch (Exception ex1) {
						logger.error(ex1.getMessage());
					}

					throw ex;

				}
				if (isStop()) {
					logger.info("stopping Sbs2JmsExchanger");
					break;
				}
			}

		} catch (Exception ex) {
			setLastexception(ex);
			logger.error(ex.getMessage(), ex);
		} finally {
			setStop(false);
			setRunning(false);
			disconnect();
		}
	}

}
