package com.jawise.sb2jmsbridge;

 class MessageExchangerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MessageExchangerException() {
		super();
	}

	public MessageExchangerException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageExchangerException(String message) {
		super(message);
	}

	public MessageExchangerException(Throwable cause) {
		super(cause);
	}

}
