package com.jawise.sb2jmsbridge;

import java.util.List;

import javax.jms.Connection;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * exchange the message from sbs to jms
 * 
 * @author sathyan
 * 
 */
class Sb2JmsExchanger extends MessageExchanger implements
		ExceptionListener {
	protected static Logger logger = Logger.getLogger(Sb2JmsExchanger.class);
	protected DataSource dSource;

	protected Connection qConnection = null;
	protected Session qSession = null;
	protected Queue queue = null;
	protected MessageProducer messageProducer = null;	

	public Sb2JmsExchanger(SbQueueConfiguration sbsconfig,
			JmsQueueConfiguration jmsconfig, DataSource dSource) {
		super(sbsconfig, jmsconfig);
		this.dSource = dSource;
	}

	public Sb2JmsExchanger(SbQueueConfiguration sbsconfig,
			JmsQueueConfiguration jmsconfig) {
		super(sbsconfig, jmsconfig);
	}

	@Override
	public void startExchange() throws MessageExchangerException {
		try {
			super.startExchange();
		} catch (Exception ex) {
			setLastexception(ex);
			throw new MessageExchangerException(ex);
		}
	}

	@Override
	public void stopExchange() {
		super.stopExchange();
		//disconnect();
	}

	@Override
	public void run() {

		try {
			setRunning(true);
			SbMessageReceiver receiver = new SbMessageReceiver(dSource,
					getSbconfiguaration());
			connect();

			DefaultTransactionDefinition txdef = new DefaultTransactionDefinition();
			Sb2JmsBridge instance = Sb2JmsBridge.getInstance();
			DataSourceTransactionManager txManneger = instance.getLocalTransactionManager();

			for (;;) {
				// get message from receiver
				Thread.sleep(100);
				TransactionStatus txstatus = txManneger.getTransaction(txdef);
				
				try {
					// should have begin transaction here
					List<SbMessage> messages = receiver.receive(10000);
					if (messages != null) {
						for (SbMessage msg : messages) {
							// then each message is send to the JMS
							// qSession = qConnection.createSession(false, 0);
							TextMessage textmsg = qSession.createTextMessage();
							createJmsMessage(msg, textmsg);
							messageProducer.send(textmsg);
							// qSession.close();
							logger
									.debug("send message to jms queue :"
											+ getJmsconfiguration().getQueue()
											+ " queuingorder :"
											+ msg.getQueuingorder());
						}
						qSession.commit();
					}
					txManneger.commit(txstatus);
				} catch (Exception ex) {
					logger.debug("rolledback faild exchange");
					try {
						if (txstatus.isNewTransaction()) {
							// her we can send it off to a DLQ instead of 
							//rollback which will cause SB to stop after 3							
							txManneger.rollback(txstatus);
							qSession.rollback();
						}
					} catch (Exception ex1) {
						logger.error(ex1.getMessage());
					}
					throw ex;

				}
				if (isStop()) {
					logger.info("stopping Sbs2JmsExchanger");
					break;
				}
			}

		} catch (Exception ex) {
			setLastexception(ex);
			logger.error(ex.getMessage(), ex);			
		} finally {
			setStop(false);
			setRunning(false);
			disconnect();
		}

	}

	protected void disconnect() {
		logger.info("disconnecting from jms provider");
		try {
			if (messageProducer != null) {
				messageProducer.close();
			}
			if (qSession != null) {
				qSession.close();
			}
			if (qConnection != null) {
				qConnection.close();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
	}

	protected void connect() throws JMSException {
		logger.info("connecting to jms provider");
		Sb2JmsBridge sbsJmsBridge = Sb2JmsBridge.getInstance();
		QueueConnectionFactory qConnectionFactory = sbsJmsBridge
				.getQueueConnectionFactory();
		qConnection = qConnectionFactory.createConnection();
		qSession = qConnection.createSession(true, 0);
		queue = qSession.createQueue(getJmsconfiguration().getQueue());
		messageProducer = qSession.createProducer(queue);
	}

	protected void createJmsMessage(SbMessage msg, TextMessage m)
			throws JMSException {
		m.setText(msg.getMessagebody());
		m.setStringProperty("messagetypename", msg.getMessagetypename());
		m.setStringProperty("conversationhandle", msg.getConversationhandle());
		m.setStringProperty("conversationgroup", msg.getConversationgroup());
		m.setStringProperty("queuingorder", msg.getQueuingorder());
		m.setStringProperty("messagesequencenumber",msg.getMessagesequencenumber());
	}

	@Override
	public void onException(JMSException ex) {
		logger.error(ex.getMessage(), ex);
		setRunning(false);
	}
}
