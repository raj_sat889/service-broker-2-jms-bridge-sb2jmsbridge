package com.jawise.sb2jmsbridge;

public class MessageExchangeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MessageExchangeException() {
		super();
	}

	public MessageExchangeException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageExchangeException(String message) {
		super(message);
	}

	public MessageExchangeException(Throwable cause) {
		super(cause);
	}

}
