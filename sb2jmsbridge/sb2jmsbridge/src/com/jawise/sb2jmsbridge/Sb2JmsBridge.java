package com.jawise.sb2jmsbridge;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.QueueConnectionFactory;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.transaction.jta.JtaTransactionManager;

public class Sb2JmsBridge {

	private static final String SPRING_FILE_CLASS_PATH = "com.jawise.config.xml";

	private static final String ARG_SPRINGFILE = "-springfile";

	private static final String VERSION = "1.0.5";

	private static Logger logger = Logger.getLogger(Sb2JmsBridge.class);

	private static Sb2JmsBridge singleton;

	private static String datasourcePassword;

	private int exitcode = 0;

	private DataSource dataSource;

	@SuppressWarnings("unchecked")
	private Map jmstosbmappings;

	// @SuppressWarnings("unchecked")
	@SuppressWarnings("unchecked")
	private Map sbtojmsmappings;

	private QueueConnectionFactory queueConnectionFactory;

	private JndiObjectFactoryBean jndiQueueConnectionFactory;

	private int exchangetreads;

	private List<MessageExchanger> exchangers;

	private int monitoringinterval;

	private boolean monitor;

	private boolean monitorrunning;

	private Properties jndienvironment;

	private JtaTransactionManager xaTransactionManager;

	private DataSourceTransactionManager localTransactionManager;

	private boolean enablexa;

	private String useremail;

	private String licensekey;

	private boolean licensevalided;

	private boolean processingStopped;

	private boolean usesystemexit;

	private Collection exiterrors;

	public String getUseremail() {
		return useremail;
	}

	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	public String getLicensekey() {
		return licensekey;
	}

	public void setLicensekey(String licensekey) {
		this.licensekey = licensekey;
	}

	public boolean isEnablexa() {
		return enablexa;
	}

	public void setEnablexa(boolean enablexa) {
		this.enablexa = enablexa;
	}

	/**
	 * retun the singleton instance of the sbsjmsbridge
	 * 
	 * @return
	 */
	public static Sb2JmsBridge getInstance() {
		if (singleton == null) {
			singleton = new Sb2JmsBridge();
		}
		return singleton;
	}

	private Sb2JmsBridge() {
		exchangers = new ArrayList<MessageExchanger>();
	}

	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Sb2JmsBridge.start(args);
	}

	/**
	 * Start the SbsJmsBridge processing
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void start(String[] args) throws Exception {

		try {
			Resource resource = null;
			String filelocation = null;

			if (args.length >= 2) {
				if (ARG_SPRINGFILE.equals(args[0])) {
					filelocation = args[1];
					resource = new FileSystemResource(new File(filelocation));
				}
			}
			if (resource == null) {
				resource = new ClassPathResource(SPRING_FILE_CLASS_PATH);
			}

			if (args.length >= 4) {
				if ("-pwd".equals(args[2])) {
					datasourcePassword = args[3];
				}
			}
			XmlBeanFactory factory = new XmlBeanFactory(resource);

			
			PropertyPlaceholderConfigurer cfg = new PropertyPlaceholderConfigurer();
			cfg.postProcessBeanFactory(factory);
			
			@SuppressWarnings("unused")
			Sb2JmsBridge sbsJmsBridge = (Sb2JmsBridge) factory
					.getBean("Sb2JmsBridge");

			sbsJmsBridge.licensevalided = false;

			if (!sbsJmsBridge.isLicenceValidate()) {
				logger
						.info("Sb2JmsBridge running in trial mode and will stop in 30 minutes");
			}

			sbsJmsBridge.initQueueConnectionFactory();
			sbsJmsBridge.startMessageExchange();
			logger.info("Sb2JmsBridge " + VERSION + " started");
			Thread.sleep(5000);
			sbsJmsBridge.monitorMessageExchangers();
			return;
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			throw ex;
		}
	}

	/**
	 * Stop processing
	 * 
	 * @param args
	 * @throws Exception
	 */

	public static void stop(String[] args) throws Exception {
		Sb2JmsBridge sb2jmsbridge = getInstance();
		sb2jmsbridge.monitor = false;
		while (sb2jmsbridge.monitorrunning) {
			Thread.sleep(100);
		}
		sb2jmsbridge.stopMessageExchnage();
		sb2jmsbridge.processingStopped = true;
		logger.info("Sb2JmsBridge stopped");
		if (sb2jmsbridge.usesystemexit) {
			System.exit(sb2jmsbridge.exitcode);
		}
	}

	public void stopMessageExchnage() {
		try {
			for (MessageExchanger exchanger : exchangers) {
				exchanger.stopExchange();
			}
			exchangers.clear();
		} catch (Exception ex) {
			logger.error("", ex);
		}

	}

	/**
	 * start the message processing
	 */
	@SuppressWarnings("unchecked")
	private void startMessageExchange() {
		try {

			Iterator iterator = sbtojmsmappings.keySet().iterator();
			while (iterator.hasNext()) {
				JmsQueueConfiguration jms = (JmsQueueConfiguration) iterator
						.next();
				SbQueueConfiguration sbs = (SbQueueConfiguration) sbtojmsmappings
						.get(jms);
				for (int i = 0; i < exchangetreads; i++) {
					MessageExchanger exchanger = null;
					exchanger = createExchanger(sbs, jms);
					exchanger.startExchange();
					exchangers.add(exchanger);
				}
			}

			iterator = jmstosbmappings.keySet().iterator();
			while (iterator.hasNext()) {
				JmsQueueConfiguration jms = (JmsQueueConfiguration) iterator
						.next();
				SbQueueConfiguration sbs = (SbQueueConfiguration) jmstosbmappings
						.get(jms);
				for (int i = 0; i < exchangetreads; i++) {
					MessageExchanger exchanger = null;
					exchanger = createExchanger(jms, sbs);
					exchanger.startExchange();
					exchangers.add(exchanger);
				}
			}
			logger.debug("started message exchangers");
		} catch (MessageExchangerException ex) {
			logger.error("", ex);
		}
	}

	private MessageExchanger createExchanger(JmsQueueConfiguration jqc,
			SbQueueConfiguration sbqc) {
		MessageExchanger exchanger;
		if (enablexa) {
			exchanger = new XAJms2SbExchanger(sbqc, jqc, dataSource);
		} else {
			exchanger = new Jms2SbExchanger(sbqc, jqc, dataSource);
		}
		return exchanger;
	}

	private MessageExchanger createExchanger(SbQueueConfiguration sbqc,
			JmsQueueConfiguration jqc) {
		MessageExchanger exchanger;
		if (enablexa) {
			exchanger = new XASb2JmsExchanger(sbqc, jqc, dataSource);
		} else {
			exchanger = new Sb2JmsExchanger(sbqc, jqc, dataSource);
		}
		return exchanger;
	}

	private void initQueueConnectionFactory() throws NamingException {
		if (queueConnectionFactory == null) {
			logger.info("Creating jndi context");

			InitialContext ctx = new InitialContext(jndienvironment);
			logger.info("Looking up connection factory");
			queueConnectionFactory = (QueueConnectionFactory) ctx
					.lookup(jndienvironment.getProperty("lookupname"));
		}
	}

	private void monitorMessageExchangers() {

		logger.debug("monitoring message exchange");
		monitor = true;
		monitorrunning = true;
		long starttime = System.currentTimeMillis();
		while (monitor) {

			if (!isLicenceValidate() && monitoringinterval > 60000) {
				monitoringinterval = 60000;
				logger
						.info("monitoring interval is limited to 60secs max in trial version");
			}

			try {
				for (MessageExchanger exchanger : exchangers) {
					if (!exchanger.isRunning()) {
						exchanger.stopExchange();
						if (exitOnError(exchanger.getLastexception())) {
							logger.info("Stopping Sb2JmsBridge");
							monitorrunning = false;
							exitcode = -1;
							Sb2JmsBridge.stop(null);
							return;
						} else {
							exchanger.startExchange();
						}
					}
					Thread.sleep(monitoringinterval);
				}

				if (exchangers.size() == 0)
					return;

			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}

			trialValidation(starttime);

		}
		monitorrunning = false;
	}

	@SuppressWarnings( { "unused", "unchecked" })
	private boolean exitOnError(Exception lastexception) {
		if (lastexception == null || lastexception.getMessage() == null) {
			return false;
		}

		if (exiterrors == null) {
			return false;
		}

		String thiserror = lastexception.getMessage();

		Iterator iter = exiterrors.iterator();
		while (iter.hasNext()) {
			String error = (String) iter.next();

			if (error != null) {
				Pattern errorpattern = Pattern.compile(error);
				Matcher errorpatternmatcher = errorpattern.matcher(thiserror);
				boolean matches = errorpatternmatcher.matches();				
				logger.debug("error pattern : " + error + ", error : "
						+ thiserror);
				logger.debug("error pattern match ? " +  (matches ? "yes" : "no"));				
				if (matches){
					return true;
				}
			}
		}
		return false;

	}

	private void trialValidation(long starttime) {
		if (!isLicenceValidate() && !processingStopped) {
			if (System.currentTimeMillis() > (starttime + (60000 * 30))) {
				logger
						.info("stoping sb2jms bridge processing, this is a trial version");
				try {
					monitorrunning = false;
					usesystemexit = false;
					Sb2JmsBridge.stop(null);
					logger.info("sb2jms bridge processing stoped");
					monitor = true;
					Thread.sleep(0);
				} catch (Exception ex) {
					logger.error(ex.getMessage(), ex);
				}
			}
		}
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public QueueConnectionFactory getQueueConnectionFactory() {
		return queueConnectionFactory;
	}

	public void setQueueConnectionFactory(
			QueueConnectionFactory queueConnectionFactory) {
		this.queueConnectionFactory = queueConnectionFactory;
	}

	public int getExchangetreads() {
		return exchangetreads;
	}

	public void setExchangetreads(int exchangetreads) {
		this.exchangetreads = exchangetreads;
	}

	@SuppressWarnings("unchecked")
	public Map getSbtojmsmappings() {
		return sbtojmsmappings;
	}

	@SuppressWarnings("unchecked")
	public void setSbtojmsmappings(Map sbtojmsmappings) {
		this.sbtojmsmappings = sbtojmsmappings;
	}

	@SuppressWarnings("unchecked")
	public Map getJmstosbmappings() {
		return jmstosbmappings;
	}

	@SuppressWarnings("unchecked")
	public void setJmstosbmappings(Map jmstosbmappings) {
		this.jmstosbmappings = jmstosbmappings;
	}

	public int getMonitoringinterval() {
		return monitoringinterval;
	}

	public void setMonitoringinterval(int monitoringinterval) {
		this.monitoringinterval = monitoringinterval;
	}

	public JndiObjectFactoryBean getJndiQueueConnectionFactory() {
		return jndiQueueConnectionFactory;
	}

	public void setJndiQueueConnectionFactory(
			JndiObjectFactoryBean jndiQueueConnectionFactory) {
		this.jndiQueueConnectionFactory = jndiQueueConnectionFactory;
	}

	public Properties getJndienvironment() {
		return jndienvironment;
	}

	public void setJndienvironment(Properties jndienvironment) {
		this.jndienvironment = jndienvironment;
	}

	public JtaTransactionManager getXaTransactionManager() {
		return xaTransactionManager;
	}

	public void setXaTransactionManager(
			JtaTransactionManager xaTransactionManager) {
		this.xaTransactionManager = xaTransactionManager;
	}

	public DataSourceTransactionManager getLocalTransactionManager() {
		return localTransactionManager;
	}

	public void setLocalTransactionManager(
			DataSourceTransactionManager localTransactionManager) {
		this.localTransactionManager = localTransactionManager;
	}

	private boolean isLicenceValidate() {
		return true;
	}

	public static String getDatasourcePassword() {
		return datasourcePassword;
	}

	public boolean getUsesystemexit() {
		return usesystemexit;
	}

	public void setUsesystemexit(boolean usesystemexit) {
		this.usesystemexit = usesystemexit;
	}

	public Collection getExiterrors() {
		return exiterrors;
	}

	public void setExiterrors(Collection exiterrors) {
		this.exiterrors = exiterrors;
	}

}
