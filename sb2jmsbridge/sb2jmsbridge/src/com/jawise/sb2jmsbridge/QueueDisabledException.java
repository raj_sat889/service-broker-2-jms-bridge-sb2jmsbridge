package com.jawise.sb2jmsbridge;

class QueueDisabledException extends  MessageExchangeException {

	public QueueDisabledException() {
		super();

	}

	public QueueDisabledException(String message, Throwable cause) {
		super(message, cause);
	}

	public QueueDisabledException(String message) {
		super(message);
	}

	public QueueDisabledException(Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
