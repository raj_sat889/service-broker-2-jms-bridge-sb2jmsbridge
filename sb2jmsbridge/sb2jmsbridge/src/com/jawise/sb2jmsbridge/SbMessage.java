package com.jawise.sb2jmsbridge;

class SbMessage {

	public static final String MSG_TYPE_END_DIALOG = "http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog";
	public static final String MSG_TYPE_BROKER_ERROR = "http://schemas.microsoft.com/SQL/ServiceBroker/Error";

	private String queuingorder;
	private String conversationhandle;
	private String conversationgroup;
	private String messagetypename;
	private String messagebody;
	private String endconversation;
	private String messagesequencenumber;

	public String getMessagesequencenumber() {
		return messagesequencenumber;
	}


	public void setMessagesequencenumber(String messagesequencenumber) {
		this.messagesequencenumber = messagesequencenumber;
	}


	public SbMessage() {
	}

	
	public SbMessage(String queuingorder, String conversationhandle,
			String conversationgroup, String messagetypename, String messagebody, String messagesequencenumber) {
		super();
		this.queuingorder = queuingorder;
		this.conversationhandle = conversationhandle;
		this.conversationgroup = conversationgroup;
		this.messagetypename = messagetypename;
		this.messagebody = messagebody;
		this.messagesequencenumber = messagesequencenumber;
	}

	public String getQueuingorder() {
		return queuingorder;
	}

	public void setQueuingorder(String queuingorder) {
		this.queuingorder = queuingorder;
	}

	public String getConversationhandle() {
		return conversationhandle;
	}

	public void setConversationhandle(String conversationhandle) {
		this.conversationhandle = conversationhandle;
	}

	public String getConversationgroup() {
		return conversationgroup;
	}

	public void setConversationgroup(String conversationgroup) {
		this.conversationgroup = conversationgroup;
	}

	public String getMessagetypename() {
		return messagetypename;
	}

	public void setMessagetypename(String messagetypename) {
		this.messagetypename = messagetypename;
	}

	public String getMessagebody() {
		return messagebody;
	}

	public void setMessagebody(String messagebody) {
		this.messagebody = messagebody;
	}


	public String getEndconversation() {
		return endconversation;
	}


	public void setEndconversation(String endconversation) {
		this.endconversation = endconversation;
	}

}
