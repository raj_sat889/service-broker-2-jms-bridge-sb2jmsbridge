package com.jawise.sb2jmsbridge;

public class SbQueueConfiguration {
	private String queue;
	private String service;
	private String messagetype;
	private String contract;
	private String fromservice;
	
	private boolean exchangeErrorMessages;
	private boolean exchangeEndDialogMessages;
	private boolean endConversationOnEndDialogMessage;
	private boolean endConversationOnErrorMessage;
	private boolean endConversationWithCleanup;
	private boolean endConversationAfterMessageSend;
	private int timeout = 10000;

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getMessagetype() {
		return messagetype;
	}

	public void setMessagetype(String messagetype) {
		this.messagetype = messagetype;
	}

	public boolean getExchangeErrorMessages() {
		return exchangeErrorMessages;
	}

	public void setExchangeErrorMessages(boolean exchangeErrorMessages) {
		this.exchangeErrorMessages = exchangeErrorMessages;
	}

	public boolean getExchangeEndDialogMessages() {
		return exchangeEndDialogMessages;
	}

	public void setExchangeEndDialogMessages(boolean exchangeEndDialogMessages) {
		this.exchangeEndDialogMessages = exchangeEndDialogMessages;
	}



	public boolean getEndConversationOnEndDialogMessage() {
		return endConversationOnEndDialogMessage;
	}

	public void setEndConversationOnEndDialogMessage(
			boolean endConversationOnEndDialogMessage) {
		this.endConversationOnEndDialogMessage = endConversationOnEndDialogMessage;
	}

	public boolean getEndConversationOnErrorMessage() {
		return endConversationOnErrorMessage;
	}

	public void setEndConversationOnErrorMessage(
			boolean endConversationOnErrorMessage) {
		this.endConversationOnErrorMessage = endConversationOnErrorMessage;
	}

	public boolean getEndConversationWithCleanup() {
		return endConversationWithCleanup;
	}

	public void setEndConversationWithCleanup(boolean endConversationWithCleanup) {
		this.endConversationWithCleanup = endConversationWithCleanup;
	}

	public boolean getEndConversationAfterMessageSend() {
		return endConversationAfterMessageSend;
	}

	public void setEndConversationAfterMessageSend(
			boolean endConversationAfterMessageSend) {
		this.endConversationAfterMessageSend = endConversationAfterMessageSend;
	}

	public String getFromservice() {
		return fromservice;
	}

	public void setFromservice(String fromservice) {
		this.fromservice = fromservice;
	}
}
