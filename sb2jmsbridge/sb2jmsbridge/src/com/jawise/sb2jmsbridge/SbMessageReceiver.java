package com.jawise.sb2jmsbridge;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * Receives messages form service broker
 * 
 * @author sathyan
 * 
 */
class SbMessageReceiver {

	private static Logger logger = Logger.getLogger(SbMessageReceiver.class);
	private DataSource dataSource;
	private SbQueueConfiguration queueconfiguration;

	public SbMessageReceiver(DataSource dataSource,
			SbQueueConfiguration queueconfiguration) {
		this.dataSource = dataSource;
		this.queueconfiguration = queueconfiguration;
	}

	public List<SbMessage> receive(int timeout) throws MessageExchangeException {

		Connection conn = null;
		Statement state = null;
		List<SbMessage> messages = null;
		try {

			conn = DataSourceUtils.getConnection(dataSource);			
			conn.setAutoCommit(false);
			
			String sql = "WAITFOR(RECEIVE TOP (1) queuing_order, message_sequence_number , conversation_handle,conversation_group_id,message_type_name, convert(xml,message_body) as message_body FROM @queue ), TIMEOUT @timeout";
			sql = sql.replaceAll("@queue", queueconfiguration.getQueue());
			sql = sql.replaceAll("@timeout", "" + timeout);
			state = conn.createStatement();
			ResultSet rs = state.executeQuery(sql);
			while (rs.next()) {

				if (messages == null) {
					messages = new ArrayList<SbMessage>();
				}

				SbMessage msg = new SbMessage(rs.getString("queuing_order"), rs
						.getString("conversation_handle"), rs
						.getString("conversation_group_id"), rs
						.getString("message_type_name"), rs
						.getString("message_body"), rs
						.getString("message_sequence_number"));
				
				logger.debug("received msg - handle : "
						+ msg.getConversationhandle() + " type : "
						+ msg.getMessagetypename());

				if (SbMessage.MSG_TYPE_END_DIALOG.equals(msg
						.getMessagetypename())) {
					if (queueconfiguration.getExchangeEndDialogMessages()) {
						messages.add(msg);
					}
					if (queueconfiguration
							.getEndConversationOnEndDialogMessage()) {
						endConversation(state, msg.getConversationhandle());
						msg.setConversationhandle(null);
					}

				} else if (SbMessage.MSG_TYPE_BROKER_ERROR.equals(msg
						.getMessagetypename())) {
					logger.debug(rs.getString("message_body"));
					if (queueconfiguration.getExchangeErrorMessages()) {
						messages.add(msg);
					}
					if (queueconfiguration.getEndConversationOnErrorMessage()) {
						endConversation(state, msg.getConversationhandle());
						msg.setConversationhandle(null);
					}
				} else {
					messages.add(msg);
				}
			}

			DataSourceUtils.releaseConnection(conn, dataSource);
			return messages;

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			try {
				if (conn != null)
					DataSourceUtils.releaseConnection(conn, dataSource);

			} catch (Exception e) {
				logger.error(ex.getMessage(), e);
			}
			if (ex.getMessage().indexOf("is currently disabled.") >= 0) {
				throw new QueueDisabledException(ex);
			}
			else
				throw new MessageExchangeException(ex.getMessage());
		}

	}

	public List<SbMessage> receive(int timeout, Connection conn)
			throws MessageExchangeException {
		Statement state = null;
		List<SbMessage> messages = null;
		try {

			String sql = "RECEIVE TOP (1) queuing_order, message_sequence_number, conversation_handle,conversation_group_id,message_type_name, convert(xml,message_body) as message_body FROM @queue";
			sql = sql.replaceAll("@queue", queueconfiguration.getQueue());
			sql = sql.replaceAll("@timeout", "" + timeout);
			state = conn.createStatement();
			ResultSet rs = state.executeQuery(sql);
			while (rs.next()) {

				if (messages == null) {
					messages = new ArrayList<SbMessage>();
				}

				SbMessage msg = new SbMessage(rs.getString("queuing_order"), rs
						.getString("conversation_handle"), rs
						.getString("conversation_group_id"), rs
						.getString("message_type_name"), rs
						.getString("message_body"), rs
						.getString("message_sequence_number"));
				
				logger.debug("received msg - handle : "
						+ msg.getConversationhandle() + " type : "
						+ msg.getMessagetypename());

				if (SbMessage.MSG_TYPE_END_DIALOG.equals(msg
						.getMessagetypename())) {
					if (queueconfiguration.getExchangeEndDialogMessages()) {
						messages.add(msg);
					}
					if (queueconfiguration
							.getEndConversationOnEndDialogMessage()) {
						endConversation(state, msg.getConversationhandle());
						msg.setConversationhandle(null);
					}

				} else if (SbMessage.MSG_TYPE_BROKER_ERROR.equals(msg
						.getMessagetypename())) {
					logger.debug(rs.getString("message_body"));
					if (queueconfiguration.getExchangeErrorMessages()) {
						messages.add(msg);
					}
					if (queueconfiguration.getEndConversationOnErrorMessage()) {
						endConversation(state, msg.getConversationhandle());
						msg.setConversationhandle(null);
					}
				} else {
					messages.add(msg);
				}
			}
			return messages;

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			if (ex.getMessage().indexOf("is currently disabled.") >= 0) {
				throw new QueueDisabledException(ex);
			}
			else
				throw new MessageExchangeException(ex.getMessage());			
		}
	}

	private void endConversation(Statement state, String hanlde)
			throws SQLException {
		logger.debug("ending conversaion");
		if (queueconfiguration.getEndConversationWithCleanup()) {
			state
					.execute("END CONVERSATION " + "'" + hanlde
							+ "' WITH CLEANUP");
		} else {
			state.execute("END CONVERSATION " + "'" + hanlde + "'");
		}
	}
}
